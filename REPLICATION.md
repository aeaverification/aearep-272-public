# AEJPol-2018-0327.R2 entitled "The Buck Stops Where? Federalism, Uncertainty, and Investment in the Brazilian Water and Sanitation Sector," Validation and Replication results

>

You may want to consult [Unofficial Verification Guidance](https://social-science-data-editors.github.io/guidance/Verification_guidance.html) for additional tips and criteria.

SUMMARY
-------
> INSTRUCTION: The Data Editor will fill this part out. It will be based on any [REQUIRED] and [SUGGESTED] action items that the report makes a note of. 

> INSTRUCTION: ALWAYS do "Data description", "Code description". If data is present, ALWAYS do "Data checks". If time is sufficient (initial assessment!), do "Replication steps", if not, explain why not.

Data description
----------------
Data are well described in the README.

### SNIS dataset provided by the Brazilian Ministry of Cities

- Abbreviation SNIS not expanded in manuscript - should be expanded to Sistema Nacional de Informações sobre Saneamento (SNIS)
- Dataset not cited
- Location of original access not mentioned
- README has mentions, but does not provide source

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references).

> [REQUIRED] Please provide a clear description of access modality and location for this dataset (where did you download it from, who transmitted it to you and when)


### municipal-level election results data

- README identifies Source: Tribunal Superior Eleitoral (TSE)
- Dataset not cited
- Location of original access not mentioned
- Only used in Appendix? Seems not be mentioned earlier

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references).

> [REQUIRED] Please provide a clear description of access modality and location for this dataset.


### municipal-level annual mortality data

- README identifies Source: Departamento de Informática do SUS (DATASUS), Brazilian Ministry of Health identified in manuscript
  - SUS not expanded
- Dataset not cited
- Location of original access not mentioned

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references).

> [REQUIRED] Please provide a clear description of access modality and location for this dataset.


### municipal-level data on public expenditures
- README identifies Source: Ministério da Fazenda, Secretaria do Tesouro Nacional (STN)
- Dataset not cited
- Location of original access not mentioned

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references).

> [REQUIRED] Please provide a clear description of access modality and location for this dataset.


### municipal-level socioeconomic indicators from the 1970 census
- README identifies  Source: Instituto Brasileiro de Geografia e Estatistica (IBGE)
- Dataset not cited
- Location of original access not mentioned


> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references).

> [REQUIRED] Please provide a clear description of access modality and location for this dataset.



### ICPSR data deposit



- [NOTE] openICPSR metadata is excellent, thank you.


Data checks
-----------
- No PII detected (municipal-level data)
- Several datasets have no variable labels, no codebook provided.

> [REQUIRED] Please ensure that all variables have labels, and that values are explained (have formats/codebook/etc.)


Code description
----------------
There is a single Stata do file. 
- README states that Figure 1 (a map) and Table 1 (no data) have no code. 
- Manual changes are necessary to create Table 2
- No packages are mentioned in the README or the code for installation.

> [REQUIRED] Please add a setup program that installs all packages as noted above. Please specify all necessary commands. An example of a setup file can be found at https://github.com/gslab-econ/template/blob/master/config/config_stata.do

Replication steps
-----------------

1. Downloaded code from openICPSR
2. Added the config.do generating system information, added to analysis.do
3. Added config_stata.do to install any packages
4. Manually copied code for Table 2 into separate file in order to run it (`table2.do`)
5. Manually uncommented all "graph export" commands so that figures actually get saved.
6. Manually modified all file paths to conform to openICPSR structure
7. Manually created directory structure (`reg`,`gph`)

> [SUGGESTED] Use of packages that automatically write out tables is strongly encouraged (Table 2)

> [REQUIRED] Please add a setup program that installs all packages as noted above. Please specify all necessary commands. An example of a setup file can be found at https://github.com/gslab-econ/template/blob/master/config/config_stata.do

> [REQUIRED] Please ensure that the openICPSR repository accurately reflects the necessary directory structure (here: data needs to be in folder "dta" or program needs to be changed)


Computing Environment
---------------------

- BioHPC Linux server, Centos 7.6, 64 cores; 1024GB RAM; 

Findings
--------

### Tables

- Table 2 is not formatted at all, labels are not specified, Panels are not labelled. None of the numbers seem to match up. The following was produced by the code, and appears to correspond to Panel A by intent, but not by numbers:
```
.         esttab . , cells("mu_1 mu_2 b p(star)") wide nonumber

-------------------------------------------------------------------
                                                                   
                     mu_1         mu_2            b            p   
-------------------------------------------------------------------
geo_area         2418.296     1782.033     636.2627     .0547484   
transport~sp     2443.765     2813.798     -370.033     1.69e-08***
-------------------------------------------------------------------
```
- Table 3: many numbers do not match up, though they are close.
- We stopped comparing tables at this point.


### Figures
All figures line up by visual inspection.

### Appendix Figures
While we do not necessarily check Appendix Figures in detail, I note that the programs fail to produce ANY figures, because the program fails.

Appendix Figure A.1 fails:
```
replace vol_water_billed = vol_water_billed * 1000 if code7 == 2112233 & year
>  == 2008
code7 not found
```
We stopped running the code at that point.

> [REQUIRED] Please provide debugged code, addressing the issues identified in this report.


### In-Text Numbers

In-text numbers not checked.

Classification
--------------

- [ ] full replication
- [ ] full replication with minor issues
- [x] partial replication (see above)
- [ ] not able to replicate most or all of the results (reasons see above)

## Code check table

| Figure/Table # | Program     | Line Number | Replicated?  |
|----------------|-------------|-------------|--------------|
| Figure 1       | n.a.        |             | n.a.         |
| Figure 2       | analysis.do | 736         | Yes          |
| Figure 3       | analysis.do | 743         | Yes          |
| Figure 4       | analysis.do | 757         | Yes          |
| Figure 5       | analysis.do | 773         | Yes          |
| Table 1        | n.a.        |             | n.a.         |
| Table 2        | table2.do (by DE) | 200         | No           |
| Table 3        | analysis.do | 235         | No           |
| Table 4        | analysis.do | 274         | not compared |
| Table 5        | analysis.do | 323         | not compared |
| Table 6        | analysis.do | 376         | not compared |
| Table 7        | analysis.do | 461         | not compared |