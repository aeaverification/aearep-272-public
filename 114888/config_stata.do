clear all
set more off

program main
    * *** Add required packages from SSC to this list ***
    local ssc_packages estout outreg2 reghdfe ftools
    * *** Add required packages from SSC to this list ***

    if !missing("`ssc_packages'") {
        foreach pkg in `ssc_packages' {
            dis "Installing `pkg'"
            ssc install `pkg', replace
        }
    }

    * Install packages using net
    * quietly net from "https://raw.githubusercontent.com/gslab-econ/stata-misc/master/"
    net install grc1leg, from(http://www.stata.com/users/vwiggins)
end

main
reghdfe, compile
