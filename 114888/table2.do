													******************************
													******************************
*---------------------------------------------------**		PRELIMINARIES		**---------------------------------------------------*
													******************************
													******************************
clear 
clear matrix
clear mata
set maxvar 32767
set matsize 11000
set emptycells drop
include "config.do"

eststo clear

* cd DIRECTORY_PATH_HERE
* local dropbox DROPBOX_PATH_HERE
local dropbox : pwd


**************************************************************************
*** TABLE 2 - Municipality Characteristics in 1970 by WS Company Type ***
**************************************************************************
****To create table 2, run this code seperately:
use "`dropbox'/1970_data.dta", clear

**Take log of variables:
foreach x in pop_total pop_rural pop_urban{
	gen log_`x' = log(1 + `x')
}
foreach x of varlist muni_gdp - gdp_other{
	gen log_`x' = log(1 + `x')
}
gen percap = muni_gdp / pop_total
gen log_percap = log(1 + percap)
gen frac_urban = pop_urban/pop_total
gen frac_rural = pop_rural/pop_total
*
**Create Balance Tables:
estpost ttest geo_area transport_cost_sp, by(scope)
	esttab . , cells("mu_1 mu_2 b p(star)") wide nonumber
estpost ttest pop_total log_pop_total frac_urban, by(scope)
	esttab . , cells("mu_1 mu_2 b p(star)") wide nonumber
estpost ttest illiterate_over15 yrs_study_over11 yrs_study_less8 yrs_study_less4 truancy_7to14 truancy_10to14, by(scope)
	esttab . , cells("mu_1 mu_2 b p(star)") wide nonumber
estpost ttest hh_electricity hh_over2_bedroom hh_toilets hh_piped_water hh_potable_water, by(scope)
	esttab . , cells("mu_1 mu_2 b p(star)") wide nonumber
estpost ttest life_expectancy infant_mortality hdi, by(scope)
	esttab . , cells("mu_1 mu_2 b p(star)") wide nonumber
estpost ttest Median_hh_income wages_farming Theil_inequality, by(scope)
	esttab . , cells("mu_1 mu_2 b p(star)") wide nonumber
estpost ttest muni_gdp log_muni_gdp log_percap log_gdp_agriculture log_gdp_construction log_gdp_industry log_gdp_services, by(scope)
	esttab . , cells("mu_1 mu_2 b p(star)") wide nonumber

