		(1)	(2)	(3)	(4)	(5)
VARIABLES	LABELS	\shortstack{Number of Water \ Connections - Total}	\shortstack{Number of Water \ Connections - Active}	\shortstack{Number of Water \ Connections - Metered}	\shortstack{Number of Households \ with Water Connection}	Water Network Length
						
muni_companyxyear_plus	Municipal WSS * Year+2	2,701***	2,064***	2,517***	2,765***	33.50***
		(779.7)	(766.5)	(719.0)	(973.6)	(7.917)
						
Observations		14,260	14,372	14,366	14,363	14,369
Year FE		Yes	Yes	Yes	Yes	Yes
Municipality FE		Yes	Yes	Yes	Yes	Yes
Adj. Within R-squared		0.647	0.605	0.653	0.701	0.293
Mean Dep. Var.		20432	18516	16764	24937	224
