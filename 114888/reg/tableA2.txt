		(1)	(2)	(3)	(4)	(5)	(6)	(7)
VARIABLES	LABELS	Total Investment	Self-Financing	Loans and Debt	Government Grants	Investment in Water	Investment in Sewer	Other Investments
								
muni_companyxlaw	Self-run company, Post-reform	2,868**	1,798***	2,124**	-92.68	521.3	1,869**	431.2***
		(1,319)	(490.3)	(921.1)	(298.0)	(599.2)	(856.4)	(147.5)
								
Observations		14,460	14,460	14,460	14,460	14,460	14,460	14,460
Year FE		Yes	Yes	Yes	Yes	Yes	Yes	Yes
Municipality FE		Yes	Yes	Yes	Yes	Yes	Yes	Yes
Adj. Within R-squared		0.299	0.0324	0.0215	0.0493	0.258	0.196	0.0238
Mean Dep. Var.		2731	717	535	395	1074	1321	192
