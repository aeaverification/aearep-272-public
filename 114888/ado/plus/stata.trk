* 00000005
*! version 1.0.0
* Do not erase or edit this file
* It is used by Stata to track the ado and help
* files you have installed.

S http://fmwww.bc.edu/repec/bocode/e
N estout.pkg
D 19 Nov 2019
U 1
d 'ESTOUT': module to make regression tables
d 
d  estout produces a table of regression results from one or
d several models for use with spreadsheets, LaTeX, HTML, or a
d word-processor table. eststo stores a quick copy of the active
d estimation results for later tabulation. esttab is a wrapper for
d estout. It displays a pretty looking publication-style regression
d table without much typing. estadd adds additional results to the
d e()-returns for one or several models previously fitted and
d stored. This package subsumes the previously circulated esto,
d esta, estadd,  and estadd_plus. An earlier version of estout is
d available  as estout1.
d 
d KW: estimates
d KW: LaTeX
d KW: HTML
d KW: word processor
d KW: output
d 
d Requires: Stata version 8.2
d 
d Distribution-Date: 20190613
d 
d Author: Ben Jann, University of Bern
d Support: email jann@@soz.unibe.ch
d 
f _/_eststo.ado
f _/_eststo.hlp
f e/estadd.ado
f e/estadd.hlp
f e/estout.ado
f e/estout.hlp
f e/eststo.ado
f e/eststo.hlp
f e/estpost.ado
f e/estpost.hlp
f e/esttab.ado
f e/esttab.hlp
e
S http://fmwww.bc.edu/repec/bocode/o
N outreg2.pkg
D 19 Nov 2019
U 2
d 'OUTREG2': module to arrange regression outputs into an illustrative table
d 
d  outreg2 provides a fast and easy way to produce an illustrative
d table  of regression outputs. The regression outputs are produced
d piecemeal and are  difficult to compare without some type of
d rearrangement. outreg2  automates this process by concatenating
d the successive regression outputs in a  vertical format. The
d resulting table is saved to the disk in ASCII  format, which can
d be read by other programs. outreg2 thus facilitates  the
d convertion of regression outputs to a standard format suitable
d for  inclusion in a scholarly publication. The functionality of
d outreg2 is based on the earlier package outreg,  by John Luke
d Gallup. Unlike outreg, outreg2 is capable of writing LaTeX-format
d tables, as well as ASCII, MS Word and MS Excel.
d 
d KW: regression
d KW: output
d KW: tables
d KW: tab-delimited output
d KW: LaTeX
d KW: Word
d KW: Excel
d 
d Requires: Stata version 7
d 
d Distribution-Date: 20140817
d 
d Author: Roy Wada
d Support: email roywada@@hotmail.com
d 
f o/outreg2.ado
f o/outreg2_prf.ado
f o/outreg2.hlp
f s/shellout.ado
f s/shellout.hlp
f s/seeout.ado
f s/seeout.hlp
e
S http://fmwww.bc.edu/repec/bocode/r
N reghdfe.pkg
D 19 Nov 2019
U 3
d 'REGHDFE': module to perform linear or instrumental-variable regression absorbing any number of high-dimensional fixed effects
d 
d  reghdfe fits a linear or instrumental-variable regression
d absorbing an arbitrary number of categorical factors and
d factorial interactions Optionally, it saves the estimated fixed
d effects.
d 
d KW: regression
d KW: instrumental variables
d KW: fixed effects
d KW: high dimension fixed effects
d 
d Requires: Stata version 11.2
d 
d Distribution-Date: 20191118
d 
d Author: Sergio Correia, Board of Governors of the Federal Reserve System
d Support: email sergio.correia@@gmail.com
d 
f r/reghdfe.ado
f r/reghdfe.mata
f r/reghdfe_old.ado
f r/reghdfe_p.ado
f r/reghdfe_old_p.ado
f r/reghdfe_estat.ado
f r/reghdfe_parse.ado
f r/reghdfe_footnote.ado
f r/reghdfe_old_estat.ado
f r/reghdfe_old_footnote.ado
f e/estfe.ado
f r/reghdfe_header.ado
f r/reghdfe_store_alphas.ado
f r/reghdfe.sthlp
f r/reghdfe_old.sthlp
f r/reghdfe_accelerations.mata
f r/reghdfe_bipartite.mata
f r/reghdfe_class.mata
f r/reghdfe_common.mata
f r/reghdfe_constructor.mata
f r/reghdfe_lsmr.mata
f r/reghdfe_projections.mata
f r/reghdfe_transforms.mata
f r/reghdfe_mata.sthlp
e
S http://www.stata.com/users/vwiggins
N grc1leg.pkg
D 19 Nov 2019
U 4
d grc1leg.  Combine graphs into one graph with a common legend.
d Program by Vince Wiggins, StataCorp <vwiggins@@stata.com>.
d Statalist distribution, 16 June 2003.
d 
d Exactly like -graph combine- but shows a single common legend for all
d combined graphs.
d 
d Distribution-Date: 02jun2010
f g/grc1leg.ado
f g/grc1leg.hlp
e
S http://fmwww.bc.edu/repec/bocode/f
N ftools.pkg
D 19 Nov 2019
U 5
d 'FTOOLS': module to provide alternatives to common Stata commands optimized for large datasets
d 
d  ftools consists of a Mata file and several Stata commands: The
d Mata file creates identifiers (factors) from variables by using
d hash functions instead of sorting the data, so it runs in time
d O(N) and not in O(N log N). The Stata commands exploit this to
d avoid sort operations,  at the cost of being slower for small
d datasets (mainly because of the cost involved in moving data from
d Stata to Mata). Implemented commands are fcollapse, fegen group,
d and fsort. Note that most of the capabilities of levels and
d contract are already supported by these commands. Possible
d commands include more egen functions and merge and reshape
d alternatives.
d 
d KW: levels
d KW: collapse
d KW: contract
d KW: egen
d KW: sort
d KW: factor variables
d KW: Mata
d 
d Requires: Stata version 11.2
d 
d Distribution-Date: 20191118
d 
d Author: Sergio Correia, Board of Governors of the Federal Reserve System
d Support: email sergio.correia@@gmail.com
d 
f f/ftools.ado
f f/ftools.mata
f f/ftools.sthlp
f f/fcollapse.ado
f f/fcollapse.sthlp
f f/fegen.ado
f f/fegen_group.ado
f f/fegen.sthlp
f f/fisid.ado
f f/fisid.sthlp
f f/flevelsof.ado
f f/flevelsof.sthlp
f f/fmerge.ado
f f/fmerge.sthlp
f f/freshape.ado
f f/fsort.ado
f f/fsort.sthlp
f f/ftab.ado
f j/join.ado
f j/join.sthlp
f l/local_inlist.ado
f l/local_inlist.sthlp
f f/fcollapse_functions.mata
f f/fcollapse_main.mata
f f/ftools_type_aliases.mata
f f/ftools.mata
f f/ftools_common.mata
f f/ftools_hash1.mata
f f/ftools_main.mata
f f/ftools_experimental.mata
f f/ftools_plugin.mata
f f/ftools_type_aliases.mata
f m/ms_compile_mata.ado
f m/ms_expand_varlist.ado
f m/ms_fvstrip.ado
f m/ms_fvstrip.sthlp
f m/ms_fvunab.ado
f m/ms_get_version.ado
f m/ms_parse_absvars.ado
f m/ms_parse_varlist.ado
f m/ms_parse_vce.ado
f m/ms_add_comma.ado
e
