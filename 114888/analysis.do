													******************************
													******************************
*---------------------------------------------------**		PRELIMINARIES		**---------------------------------------------------*
													******************************
													******************************
clear 
clear matrix
clear mata
set maxvar 32767
set matsize 11000
set emptycells drop
include "config.do"

eststo clear

* cd DIRECTORY_PATH_HERE
* local dropbox DROPBOX_PATH_HERE
local dropbox : pwd

use "`dropbox'/ws_data.dta", clear

													**********************
													**********************
*---------------------------------------------------**		SETUP		**------------------------------------------------------------*
													**********************
													**********************
*********************************
**Create Diff-in-Diff variables**
*********************************
*Drop the 10 municipalities that are defined as "Microregional"
drop if scope == "" | scope == " "

gen muni_company = .
	replace muni_company = 0 if scope == "Regional"
	replace muni_company = 1 if scope == "Local"
gen law = .
	replace law = 1 if year > 2005
	replace law = 0 if year <= 2005
gen muni_companyxlaw = muni_company * law

local diff_diff 					"muni_company law muni_companyxlaw"

label variable muni_company 		"Municipal WSS"
label variable law 					"Bill 5.296"
label variable muni_companyxlaw 	"Self-run company, Post-reform"

****************************************
**Rescale investment variables by 1000**
****************************************
replace invest_total = invest_total / 1000
replace invest_own = invest_own / 1000
replace invest_resources_large = invest_resources_large / 1000
replace invest_resources_small = invest_resources_small / 1000
replace invest_in_water = invest_in_water / 1000
replace invest_in_sewer = invest_in_sewer / 1000
replace invest_in_other = invest_in_other / 1000
replace capital_exp = capital_exp / 1000

******************************
**Label Investment Variables**
******************************
label var invest_total 				"Total Investment"
label var invest_own 				"Self Financing"
label var invest_resources_large 	"Loans and Debt"
label var invest_resources_small 	"Government Grants"
label var invest_in_water 			"Investment in Water"
label var invest_in_sewer 			"Investment in Sewer"
label var invest_in_other 			"Other Investments"
label var capital_exp 				"Capital Expenditure"

******************************************
**Creating indicators for balanced panel**
******************************************
**Drop duplicate observations (75 observations in total)
bys code year: gen n = _n
	drop if n > 1
	drop n
	
**Indicator if a municipality has observations from 2001-2012
bys code: gen m = _N
gen balance2001 = .
	replace balance2001 = 1 if m == 12
	replace balance2001 = 0 if m != 12
	drop m

**Indicator if a municipality has observations from 2004-2012
bys code: gen m = 1 if year >= 2004
bys code: gen n = sum(m)
bys code: egen n1 = max(n)
gen balance2004 = .
	replace balance2004 = 1 if n1 == 9
	replace balance2004 = 0 if n1 != 9
	replace balance2004 = 0 if year < 2004
	drop m n n1

**Indicator if municipality has at least one observation before 2006
gen n1 = .
replace n1 = 1 if year <= 2005
replace n1 = 0 if year >2005
bys code: gen n2 = sum(n1)
gen pre2005 = .
	replace pre2005 = 1 if n2 > 0
	replace pre2005 = 0 if n2 == 0
	drop n1 n2

****************************************
**Coding Top 15 Cities and Metro Areas**
****************************************
gen top15 = .
	replace top15 = 1 if code == 355030
	replace top15 = 1 if code == 330455
	replace top15 = 1 if code == 292740
	replace top15 = 1 if code == 530010
	replace top15 = 1 if code == 230440
	replace top15 = 1 if code == 310620
	replace top15 = 1 if code == 130260
	replace top15 = 1 if code == 410690
	replace top15 = 1 if code == 261160
	replace top15 = 1 if code == 431490
	replace top15 = 1 if code == 150140
	replace top15 = 1 if code == 520870
	replace top15 = 1 if code == 351880
	replace top15 = 1 if code == 350950
	replace top15 = 1 if code == 330490
	replace top15 = 0 if top15 != 1
gen metro_area = .
	replace metro_area = 1 if metro_area_name != ""
	replace metro_area = 0 if metro_area_name == ""

*****************************************
**Drop Mato Grosso the Federal District**
*****************************************
*Mato Grosso had a state company (SANEMAT) that was created in 1966, but was desolved in 1998
**Since MT has no state company observations in our sample, we'll remove it.
drop if state == "MT"
drop if state == "DF"

**************************************
**Create company specific identifer**
**************************************
**For clustering purposes, we'll want to cluster at the company level
gen company = code
	replace company = 110000 if state == "RO" & scope == "Regional"
	replace company = 120000 if state == "AC" & scope == "Regional"
	replace company = 130000 if state == "AM" & scope == "Regional"
	replace company = 140000 if state == "RR" & scope == "Regional"
	replace company = 150000 if state == "PA" & scope == "Regional"
	replace company = 160000 if state == "AP" & scope == "Regional"
	replace company = 170000 if state == "TO" & scope == "Regional"
	replace company = 210000 if state == "MA" & scope == "Regional"
	replace company = 220000 if state == "PI" & scope == "Regional"
	replace company = 230000 if state == "CE" & scope == "Regional"
	replace company = 240000 if state == "RN" & scope == "Regional"
	replace company = 250000 if state == "PB" & scope == "Regional"
	replace company = 260000 if state == "PE" & scope == "Regional"
	replace company = 270000 if state == "AL" & scope == "Regional"
	replace company = 280000 if state == "SE" & scope == "Regional"
	replace company = 290000 if state == "BA" & scope == "Regional"
	replace company = 310000 if state == "MG" & scope == "Regional"
	replace company = 320000 if state == "ES" & scope == "Regional"
	replace company = 330000 if state == "RJ" & scope == "Regional"
	replace company = 350000 if state == "SP" & scope == "Regional"
	replace company = 410000 if state == "PR" & scope == "Regional"
	replace company = 420000 if state == "SC" & scope == "Regional"
	replace company = 430000 if state == "RS" & scope == "Regional"
	replace company = 500000 if state == "MS" & scope == "Regional"
	replace company = 520000 if state == "GO" & scope == "Regional"
	order company, after(scope)

*******************************
**Preparing Control Variables**
*******************************
gen pop_log = log(muni_pop)
gen gdp_log = log(gdp / muni_pop)
gen taxes_log = log(taxes / muni_pop)

local finance_controls 				"gdp_* taxes_*"
local ag_controls 					"ag_area ag_harvest ag_value livestock"
local clim_controls 				"temper precip"

*Create initial investment interacted with time trend:
gen time = year - 2000
bys code: gen n = _n
	gen y1_invest = invest_total if n == 1
bys code: egen base_invest = sum(y1_invest)
	gen baseinvestTT = (base_invest * time)
drop n y1_invest base_invest

local controls 						"pop_log `finance_controls' `ag_controls' `clim_controls' baseinvestTT"


xtset code year

													**********************
													**********************
*---------------------------------------------------**		TABLES		**---------------------------------------------------*
													**********************
													**********************

**************************************************************************
*** TABLE 2 - Municipality Characteristics in 1970 by WS Company Type ***
**************************************************************************
****To create table 2, run this code seperately:
*use "`dropbox'/1970_data.dta", clear
*
**Take log of variables:
*foreach x in pop_total pop_rural pop_urban{
*	gen log_`x' = log(1 + `x')
*}
*foreach x of varlist muni_gdp - gdp_other{
*	gen log_`x' = log(1 + `x')
*}
*gen percap = muni_gdp / pop_total
*gen log_percap = log(1 + percap)
*gen frac_urban = pop_urban/pop_total
*gen frac_rural = pop_rural/pop_total
*
**Create Balance Tables:
*estpost ttest geo_area transport_cost_sp, by(scope)
*	esttab . , cells("mu_1 mu_2 b p(star)") wide nonumber
*estpost ttest pop_total log_pop_total frac_urban, by(scope)
*	esttab . , cells("mu_1 mu_2 b p(star)") wide nonumber
*estpost ttest illiterate_over15 yrs_study_over11 yrs_study_less8 yrs_study_less4 truancy_7to14 truancy_10to14, by(scope)
*	esttab . , cells("mu_1 mu_2 b p(star)") wide nonumber
*estpost ttest hh_electricity hh_over2_bedroom hh_toilets hh_piped_water hh_potable_water, by(scope)
*	esttab . , cells("mu_1 mu_2 b p(star)") wide nonumber
*estpost ttest life_expectancy infant_mortality hdi, by(scope)
*	esttab . , cells("mu_1 mu_2 b p(star)") wide nonumber
*estpost ttest Median_hh_income wages_farming Theil_inequality, by(scope)
*	esttab . , cells("mu_1 mu_2 b p(star)") wide nonumber
*estpost ttest muni_gdp log_muni_gdp log_percap log_gdp_agriculture log_gdp_construction log_gdp_industry log_gdp_services, by(scope)
*	esttab . , cells("mu_1 mu_2 b p(star)") wide nonumber

	
********************************
*** TABLE 3 -  WS Investment ***
********************************
quietly reghdfe invest_total muni_companyxlaw `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_total if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table3.tex", tex(frag) replace nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Total Investment) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_own muni_companyxlaw `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_own if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table3.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Self-Financing) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_resources_large muni_companyxlaw `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_resources_large if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table3.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Loans and Debt) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_resources_small muni_companyxlaw `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_resources_small if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table3.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Government Grants) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_water muni_companyxlaw `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_in_water if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table3.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Investment in Water) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_sewer muni_companyxlaw `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_in_sewer if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table3.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Investment in Sewer) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_other muni_companyxlaw `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_in_other if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table3.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Other Investments) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')


*****************************************************
*** TABLE 4 -  WS Investment by Metropolitan Area ***
*****************************************************
*Prepare metro area variables:
gen metro_yes = 0
	replace metro_yes = 1 if metro_area == 1
	replace metro_yes = 1 if muni_company == 0
gen metro_no = 0
	replace metro_no = 1 if metro_area == 0
	replace metro_no = 1 if muni_company == 0
gen muni_companyxlawxmetroYes = muni_companyxlaw  * metro_yes
gen muni_companyxlawxmetroNo = muni_companyxlaw  * metro_no

quietly reghdfe invest_total muni_companyxlawxmetroYes muni_companyxlawxmetroNo `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_total if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table4.tex", tex(frag) replace nocons nonotes nor2 keep(muni_companyxlawxmetroYes muni_companyxlawxmetroNo) label ctitle(Total Investment) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_own muni_companyxlawxmetroYes muni_companyxlawxmetroNo `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_own if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table4.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlawxmetroYes muni_companyxlawxmetroNo) label ctitle(Self-Financing) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_resources_large muni_companyxlawxmetroYes muni_companyxlawxmetroNo `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_resources_large if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table4.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlawxmetroYes muni_companyxlawxmetroNo) label ctitle(Loans and Debt) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_resources_small muni_companyxlawxmetroYes muni_companyxlawxmetroNo `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_resources_small if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table4.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlawxmetroYes muni_companyxlawxmetroNo) label ctitle(Government Grants) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_water muni_companyxlawxmetroYes muni_companyxlawxmetroNo `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_in_water if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table4.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlawxmetroYes muni_companyxlawxmetroNo) label ctitle(Investment in Water) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_sewer muni_companyxlawxmetroYes muni_companyxlawxmetroNo `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_in_sewer if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table4.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlawxmetroYes muni_companyxlawxmetroNo) label ctitle(Investment in Sewer) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_other muni_companyxlawxmetroYes muni_companyxlawxmetroNo `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_in_other if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table4.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlawxmetroYes muni_companyxlawxmetroNo) label ctitle(Other Investments) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')


******************************************************
*** TABLE 5 -  WS Investment by Share of State GDP ***
******************************************************
*Prepare gdp share variables:
gen gdp_sharehelper = .
	replace gdp_sharehelper = gdp_share_state if year == 2001
	bys code: egen gdp_share2001 = max(gdp_sharehelper) if balance2001 == 1
	drop gdp_sharehelper
gen gdpshare_high = 0
	replace gdpshare_high = 1 if gdp_share2001 > 1 & muni_company == 1
	replace gdpshare_high = 1 if muni_company == 0
gen gdpshare_low = 0
	replace gdpshare_low = 1 if gdp_share2001 <= 1 & muni_company == 1
	replace gdpshare_low = 1 if muni_company == 0
gen muni_companyxlawxgdpHigh = muni_companyxlaw  * gdpshare_high
gen muni_companyxlawxgdpLow = muni_companyxlaw  * gdpshare_low

quietly reghdfe invest_total muni_companyxlawxgdpHigh muni_companyxlawxgdpLow `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_total if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table5.tex", tex(frag) replace nocons nonotes nor2 keep(muni_companyxlawxgdpHigh muni_companyxlawxgdpLow) label ctitle(Total Investment) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_own muni_companyxlawxgdpHigh muni_companyxlawxgdpLow `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_own if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table5.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlawxgdpHigh muni_companyxlawxgdpLow) label ctitle(Self-Financing) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_resources_large muni_companyxlawxgdpHigh muni_companyxlawxgdpLow `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_resources_large if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table5.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlawxgdpHigh muni_companyxlawxgdpLow) label ctitle(Loans and Debt) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_resources_small muni_companyxlawxgdpHigh muni_companyxlawxgdpLow `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_resources_small if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table5.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlawxgdpHigh muni_companyxlawxgdpLow) label ctitle(Government Grants) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_water muni_companyxlawxgdpHigh muni_companyxlawxgdpLow `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_in_water if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table5.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlawxgdpHigh muni_companyxlawxgdpLow) label ctitle(Investment in Water) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_sewer muni_companyxlawxgdpHigh muni_companyxlawxgdpLow `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_in_sewer if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table5.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlawxgdpHigh muni_companyxlawxgdpLow) label ctitle(Investment in Sewer) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_other muni_companyxlawxgdpHigh muni_companyxlawxgdpLow `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_in_other if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table5.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlawxgdpHigh muni_companyxlawxgdpLow) label ctitle(Other Investments) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')


***********************************
*** TABLE 6 -  WS System Access ***
***********************************
gen year_plus = .
	replace year_plus = 1 if year > 2007
	replace year_plus = 0 if year <= 2007
gen muni_companyxyear_plus = muni_company * year_plus
label variable year_plus "Year+2"
label variable muni_companyxyear_plus "Municipal WSS * Year+2"
local diff_plus "muni_company year_plus muni_companyxyear_plus"

**Cleanup Access Variables:
foreach x in water_connect_total water_connect_active water_connect_meter water_hh water_hh_meter length_water{
	gen `x'_helper = .
	replace `x'_helper = 1 if `x' == 0 | `x' == .
	replace `x'_helper = . if balance2001 != 1
	bys code: gen `x'_helper_sum = sum(`x'_helper)
	bys code: egen `x'_helper_max = max(`x'_helper_sum)
	replace `x' = 0 if `x'_helper_max == 12
	drop `x'_helper `x'_helper_sum `x'_helper_max
}

foreach x in sewer_connect_total sewer_connect_active sewer_connect_meter sewer_hh length_sewer{
	gen `x'_helper = .
	replace `x'_helper = 1 if `x' == 0 | `x' == .
	replace `x'_helper = . if balance2001 != 1
	bys code: gen `x'_helper_sum = sum(`x'_helper)
	bys code: egen `x'_helper_max = max(`x'_helper_sum)
	replace `x' = 0 if `x'_helper_max == 12
	drop `x'_helper `x'_helper_sum `x'_helper_max
}

**PANEL A - Water Network
quietly reghdfe water_connect_total muni_companyxyear_plus `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum water_connect_total if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table6A.tex", tex(frag) replace nocons nonotes nor2 keep(muni_companyxyear_plus) label ctitle(\shortstack{Number of Water \\ Connections - Total}) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe water_connect_active muni_companyxyear_plus `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum water_connect_active if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table6A.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxyear_plus) label ctitle(\shortstack{Number of Water \\ Connections - Active}) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe water_connect_meter muni_companyxyear_plus `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum water_connect_meter if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table6A.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxyear_plus) label ctitle(\shortstack{Number of Water \\ Connections - Metered}) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe water_hh muni_companyxyear_plus `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum water_hh if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table6A.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxyear_plus) label ctitle(\shortstack{Number of Households \\ with Water Connection}) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe length_water muni_companyxyear_plus `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum length_water if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table6A.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxyear_plus) label ctitle(Water Network Length) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

**PANEL B - Sewer Network
quietly reghdfe sewer_connect_total muni_companyxyear_plus `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum sewer_connect_total if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table6B.tex", tex(frag) replace nocons nonotes nor2 keep(muni_companyxyear_plus) label ctitle(\shortstack{Number of Sewer \\ Connections - Total}) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe sewer_connect_active muni_companyxyear_plus `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum sewer_connect_active if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table6B.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxyear_plus) label ctitle(\shortstack{Number of Sewer \\ Connections - Active}) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe sewer_connect_meter muni_companyxyear_plus `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum sewer_connect_meter if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table6B.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxyear_plus)  label ctitle(\shortstack{Number of Sewer \\ Connections - Metered})addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe sewer_hh muni_companyxyear_plus `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum sewer_hh if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table6B.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxyear_plus) label ctitle(\shortstack{Number of Households \\ with Sewer Connection}) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe length_sewer muni_companyxyear_plus `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum length_sewer if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/table6B.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxyear_plus) label ctitle(Sewer Network Length) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')


****************************
*** TABLE 7 -  Mortality ***
****************************
merge 1:1 code year using "`dropbox'/mortality_data.dta"
	drop if _merge == 2
	drop _merge
gen mortality_04 = mortality_01 + mortality_14
gen mortality_1019 = mortality_1014 + mortality_1519

quietly reghdfe mortality_01 muni_companyxyear_plus `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
outreg2 using "`dropbox'/reg/table7.tex", tex(frag) replace nocons nonotes nor2 keep(muni_companyxyear_plus) label ctitle(Less Than 1 Year) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within))

quietly reghdfe mortality_14 muni_companyxyear_plus `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
outreg2 using "`dropbox'/reg/table7.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxyear_plus) label ctitle(1 - 4 Years) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within))

quietly reghdfe mortality_59 muni_companyxyear_plus `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
outreg2 using "`dropbox'/reg/table7.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxyear_plus) label ctitle(5 - 9 Years) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within))

quietly reghdfe mortality_1014 muni_companyxyear_plus `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
outreg2 using "`dropbox'/reg/table7.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxyear_plus) label ctitle(9 - 14 Years) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within))

quietly reghdfe mortality_1519 muni_companyxyear_plus `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
outreg2 using "`dropbox'/reg/table7.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxyear_plus) label ctitle(15 - 19 Years) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within))

quietly reghdfe mortality_2029 muni_companyxyear_plus `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
outreg2 using "`dropbox'/reg/table7.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxyear_plus) label ctitle(20 - 29 Years) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within))


													******************************
													******************************
*---------------------------------------------------**		APPENDIX TABLES		**---------------------------------------------------*
													******************************
													******************************
													
****************************************
*** TABLE A.1 - Observations by Year ***
****************************************
bys year: tab scope


****************************************************
*** TABLE A.2 - WS Investment - Unbalanced Panel ***
****************************************************
quietly reghdfe invest_total muni_companyxlaw `controls' , absorb(code year) vce(cluster company)
	sum invest_total if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA2.tex", tex(frag) replace nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Total Investment) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_own muni_companyxlaw `controls' , absorb(code year) vce(cluster company)
	sum invest_own if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA2.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Self-Financing) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_resources_large muni_companyxlaw `controls' , absorb(code year) vce(cluster company)
	sum invest_resources_large if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA2.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Loans and Debt) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_resources_small muni_companyxlaw `controls' , absorb(code year) vce(cluster company)
	sum invest_resources_small if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA2.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Government Grants) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_water muni_companyxlaw `controls' , absorb(code year) vce(cluster company)
	sum invest_in_water if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA2.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Investment in Water) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_sewer muni_companyxlaw `controls' , absorb(code year) vce(cluster company)
	sum invest_in_sewer if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA2.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Investment in Sewer) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_other muni_companyxlaw `controls' , absorb(code year) vce(cluster company)
	sum invest_in_other if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA2.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Other Investments) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')


************************************************************
*** TABLE A.3 - WS Investment - Legislation Passage Date ***
************************************************************
**Create 2007 interaction variables
gen post2007 = .
	replace post2007 = 1 if year > 2007
	replace post2007 = 0 if year <= 2007
gen muni_companyxpost2007 = muni_company * post2007
label variable post2007 "Law 11.447"
label variable muni_companyxpost2007 "Self-run company, Post-enactment"

quietly reghdfe invest_total muni_companyxpost2007 `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_total if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA3.tex", tex(frag) replace nocons nonotes nor2 keep(muni_companyxpost2007) label ctitle(Total Investment) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_own muni_companyxpost2007 `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_own if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA3.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxpost2007) label ctitle(Self-Financing) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_resources_large muni_companyxpost2007 `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_resources_large if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA3.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxpost2007) label ctitle(Loans and Debt) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_resources_small muni_companyxpost2007 `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_resources_small if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA3.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxpost2007) label ctitle(Government Grants) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_water muni_companyxpost2007 `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_in_water if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA3.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxpost2007) label ctitle(Investment in Water) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_sewer muni_companyxpost2007 `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_in_sewer if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA3.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxpost2007) label ctitle(Investment in Sewer) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_other muni_companyxpost2007 `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_in_other if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA3.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxpost2007) label ctitle(Other Investments) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')


*******************************************************
*** TABLE A.4 - WS Investment - No Linear Timetrend ***
*******************************************************
local controls_noTT 						"pop_log `finance_controls' `ag_controls' `clim_controls'"

quietly reghdfe invest_total muni_companyxlaw `controls_noTT' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_total if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA4.tex", tex(frag) replace nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Total Investment) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_own muni_companyxlaw `controls_noTT' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_own if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA4.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Self-Financing) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_resources_large muni_companyxlaw `controls_noTT' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_resources_large if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA4.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Loans and Debt) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_resources_small muni_companyxlaw `controls_noTT' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_resources_small if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA4.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Government Grants) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_water muni_companyxlaw `controls_noTT' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_in_water if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA4.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Investment in Water) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_sewer muni_companyxlaw `controls_noTT' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_in_sewer if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA4.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Investment in Sewer) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_other muni_companyxlaw `controls_noTT' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_in_other if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA4.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Other Investments) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

									
*********************************************
*** TABLE A.5 - WS Investment - N and CW  ***
*********************************************
**PANEL A - N and CW Only
preserve
keep if region == "N" | region == "CW"

quietly reghdfe invest_total muni_companyxlaw `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_total if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA5A.tex", tex(frag) replace nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Total Investment) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_own muni_companyxlaw `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_own if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA5A.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Self-Financing) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_resources_large muni_companyxlaw `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_resources_large if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA5A.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Loans and Debt) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_resources_small muni_companyxlaw `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_resources_small if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA5A.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Government Grants) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_water muni_companyxlaw `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_in_water if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA5A.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Investment in Water) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_sewer muni_companyxlaw `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_in_sewer if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA5A.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Investment in Sewer) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_other muni_companyxlaw `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_in_other if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA5A.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Other Investments) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

restore

**PANEL B - Excluding N and CW
preserve
drop if region == "N" | region == "CW"

quietly reghdfe invest_total muni_companyxlaw `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_total if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA5B.tex", tex(frag) replace nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Total Investment) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_own muni_companyxlaw `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_own if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA5B.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Self-Financing) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_resources_large muni_companyxlaw `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_resources_large if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA5B.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Loans and Debt) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_resources_small muni_companyxlaw `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_resources_small if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA5B.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Government Grants) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_water muni_companyxlaw `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_in_water if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA5B.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Investment in Water) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_sewer muni_companyxlaw `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_in_sewer if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA5B.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Investment in Sewer) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

quietly reghdfe invest_in_other muni_companyxlaw `controls' if balance2001 == 1, absorb(code year) vce(cluster company)
	sum invest_in_other if e(sample)
	local avg = int(r(mean))
outreg2 using "`dropbox'/reg/tableA5B.tex", tex(frag) append nocons nonotes nor2 keep(muni_companyxlaw) label ctitle(Other Investments) addtext(Year FE, Yes, Municipality FE, Yes) adds("Adj. Within R-squared", e(r2_a_within), "Mean Dep. Var.", `avg')

restore


********************************************
*** TABLE A.6 - MORTALITY SUMMARY STATS  ***
********************************************
***To create table A.6, run this code seperately:
* preserve
* drop if balance2001 == 0
* bys muni_company: summ mortality_01
* bys muni_company: summ mortality_14
* bys muni_company: summ mortality_59
* bys muni_company: summ mortality_1014
* bys muni_company: summ mortality_1519
* bys muni_company: summ mortality_2029

													**********************
													**********************
*---------------------------------------------------**		FIGURES		**---------------------------------------------------*
													**********************
													**********************
preserve
drop if balance2001 == 0 
collapse (mean) invest_total invest_own invest_resources_large invest_resources_small invest_in_water invest_in_sewer invest_in_other, by(muni_company year)

***********************************
*** FIGURE 2 - Total Investment ***
***********************************
twoway (line invest_total year if muni_company == 1, sort clpattern(solid)) (line invest_total year if muni_company == 0, sort clpattern(dash)), xline(2005, lpat(dot)) xlabel(2001(1)2012,labsize(small)) ylabel(,labsize(small) angle(horizontal)) ytitle(Investment (in ,000 Reals), size(small)) xtitle(Year, size(small)) legend( order(1 "Municipal-Run Company" 2 "State-Run Company") size(small)) scheme(s2mono) graphregion(fcolor(white) lcolor(black)) name(invest_total, replace) 
graph export "`dropbox'/gph/figure2.pdf", replace


***************************************
*** FIGURE 3 - Investment by Source ***
***************************************										
**PANEL A - Investment from Self-Financing
twoway (line invest_own year if muni_company == 1, sort clpattern(solid)) (line invest_own year if muni_company == 0, sort clpattern(dash)), title(Panel A: Investment from Self-Financing, size(small)) xline(2005, lpat(dot)) xlabel(2001(2)2012,labsize(small)) ylabel(,labsize(small) angle(horizontal)) ytitle(Investment (in ,000 Reals), size(small)) xtitle(Year, size(small)) legend( order(1 "Municipal-Run Company" 2 "State-Run Company") size(vsmall) rows(2) bmargin(b=15 r=15)) scheme(s2mono) graphregion(fcolor(white) lcolor(black)) name(Figure3A, replace)
**PANEL B - Investment from Loans and Debt
twoway (line invest_resources_large year if muni_company == 1, sort clpattern(solid)) (line invest_resources_large year if muni_company == 0, sort clpattern(dash)), title(Panel B: Investment from Loans and Debt, size(small)) xline(2005, lpat(dot)) xlabel(2001(2)2012,labsize(small)) ylabel(,labsize(small) angle(horizontal)) ytitle(Investment (in ,000 Reals), size(small)) xtitle(Year, size(small)) legend( order(1 "Municipal-Run Company" 2 "State-Run Company") size(small)) scheme(s2mono) graphregion(fcolor(white) lcolor(black)) name(Figure3B, replace)
**PANEL C - Investment from Government Grants
twoway (line invest_resources_small year if muni_company == 1, sort clpattern(solid)) (line invest_resources_small year if muni_company == 0, sort clpattern(dash)), title(Panel C: Investment from Government Grants, size(small)) xline(2005, lpat(dot)) xlabel(2001(2)2012,labsize(small)) ylabel(,labsize(small) angle(horizontal)) ytitle(Investment (in ,000 Reals), size(small)) xtitle(Year, size(small)) legend( order(1 "Municipal-Run Company" 2 "State-Run Company") size(small)) scheme(s2mono) graphregion(fcolor(white) lcolor(black)) name(Figure3C, replace)

grc1leg Figure3A Figure3B Figure3C, name(Figure3, replace) position(5) ring(0) scheme(s2mono) graphregion(fcolor(white) lcolor(black))
graph export "`dropbox'/gph/figure3.pdf", replace


********************************************
*** FIGURE 4 - Investment by Destination ***
********************************************
**PANEL A - Investment in Water Network
twoway (line invest_in_water year if muni_company == 1, sort clpattern(solid)) (line invest_in_water year if muni_company == 0, sort clpattern(dash)), title(Panel A: Investment in Water Network, size(small)) xline(2005, lpat(dot)) xlabel(2001(2)2012,labsize(small)) ylabel(,labsize(small) angle(horizontal)) ytitle(Investment (in ,000 Reals), size(small)) xtitle(Year, size(small)) legend( order(1 "Municipal-Run Company" 2 "State-Run Company") size(vsmall) rows(2) bmargin(b=15 r=15)) scheme(s2mono) graphregion(fcolor(white) lcolor(black)) name(Figure4A, replace)
**PANEL B - Investment in Sewer Network
twoway (line invest_in_sewer year if muni_company == 1, sort clpattern(solid)) (line invest_in_sewer year if muni_company == 0, sort clpattern(dash)), title(Panel B: Investment in Sewer Network, size(small)) xline(2005, lpat(dot)) xlabel(2001(2)2012,labsize(small)) ylabel(,labsize(small) angle(horizontal)) ytitle(Investment (in ,000 Reals), size(small)) xtitle(Year, size(small)) legend( order(1 "Municipal-Run Company" 2 "State-Run Company") size(small)) scheme(s2mono) graphregion(fcolor(white) lcolor(black)) name(Figure4B, replace)
**PANEL C - Other Network Investments
twoway (line invest_in_other year if muni_company == 1, sort clpattern(solid)) (line invest_in_other year if muni_company == 0, sort clpattern(dash)), title(Panel C: Other Network Investments, size(small)) xline(2005, lpat(dot)) xlabel(2001(2)2012,labsize(small)) ylabel(,labsize(small) angle(horizontal)) ytitle(Investment (in ,000 Reals), size(small)) xtitle(Year, size(small)) legend( order(1 "Municipal-Run Company" 2 "State-Run Company") size(small)) scheme(s2mono) graphregion(fcolor(white) lcolor(black)) name(Figure4C, replace)

grc1leg Figure4A Figure4B Figure4C, name(Figure4, replace) position(5) ring(0) scheme(s2mono) graphregion(fcolor(white) lcolor(black))
graph export "`dropbox'/gph/figure4.pdf", replace

restore


*************************************
*** FIGURE 5 - Other Public Goods ***
*************************************
merge 1:1 code year using "`dropbox'/pub_exp_data.dta"
	drop if _merge == 2
	drop _merge

preserve
drop if balance2001 == 0 
drop if year > 2010
collapse (mean) costs_welfare costs_education costs_housing costs_transport costs_administration, by(muni_company year)


**PANEL A - Welfare Programs
twoway (line costs_welfare year if muni_company == 1, sort clpattern(solid)) (line costs_welfare year if muni_company == 0, sort clpattern(dash)), title(Panel A: Assistance and Welfare Programs, size(small)) xline(2005, lpat(dot)) xlabel(2001(1)2010,labsize(small)) ylabel(,labsize(small) angle(horizontal)) ytitle(Expenditure (in ,000 Reals), size(small)) xtitle(Year, size(small)) legend( order(1 "Self-Run Municipalities" 2 "State-Run Municipalities") size(vsmall) rows(3) bmargin(b=15 r=15)) scheme(s2mono) graphregion(fcolor(white) lcolor(black)) name(Figure5A, replace)
**PANEL B - Education
twoway (line costs_education year if muni_company == 1, sort clpattern(solid)) (line costs_education year if muni_company == 0, sort clpattern(dash)), title(Panel B: Education and Cultural Programs, size(small)) xline(2005, lpat(dot)) xlabel(2001(1)2010,labsize(small)) ylabel(,labsize(small) angle(horizontal)) ytitle(Expenditure (in ,000 Reals), size(small)) xtitle(Year, size(small)) legend( order(1 "Self-Run Municipalities" 2 "State-Run Municipalities") size(small)) scheme(s2mono) graphregion(fcolor(white) lcolor(black)) name(Figure5B, replace)
**PANEL C - Housing
twoway (line costs_housing year if muni_company == 1, sort clpattern(solid)) (line costs_housing year if muni_company == 0, sort clpattern(dash)), title(Panel C: Housing and Urban Development, size(small)) xline(2005, lpat(dot)) xlabel(2001(1)2010,labsize(small)) ylabel(,labsize(small) angle(horizontal)) ytitle(Expenditure (in ,000 Reals), size(small)) xtitle(Year, size(small)) legend( order(1 "Self-Run Municipalities" 2 "State-Run Municipalities") size(small)) scheme(s2mono) graphregion(fcolor(white) lcolor(black)) name(Figure5C, replace)
**PANEL D - Administration
twoway (line costs_administration year if muni_company == 1 & year > 2001, sort clpattern(solid)) (line costs_administration year if muni_company == 0 & year > 2001, sort clpattern(dash)), title(Panel D: Municipal Management and Planning, size(small)) xline(2005, lpat(dot)) xlabel(2002(1)2010,labsize(small)) ylabel(,labsize(small) angle(horizontal)) ytitle(Expenditure (in ,000 Reals), size(small)) xtitle(Year, size(small)) legend( order(1 "Self-Run Municipalities" 2 "State-Run Municipalities") size(small)) scheme(s2mono) graphregion(fcolor(white) lcolor(black)) name(Figure5D, replace)
**PANEL E - Transportation
twoway (line costs_transport year if muni_company == 1, sort clpattern(solid)) (line costs_transport year if muni_company == 0, sort clpattern(dash)), title(Panel E: Transportation, size(small)) xline(2005, lpat(dot)) xlabel(2001(1)2010,labsize(small)) ylabel(,labsize(small) angle(horizontal)) ytitle(Expenditure (in ,000 Reals), size(small)) xtitle(Year, size(small)) legend( order(1 "Self-Run Municipalities" 2 "State-Run Municipalities") size(small)) scheme(s2mono) graphregion(fcolor(white) lcolor(black)) name(Figure5E, replace)

grc1leg Figure5A Figure5B Figure5C Figure5D Figure5E, name(Figure5, replace) xsize(50) ysize(7) position(5) ring(0) scheme(s2mono) graphregion(fcolor(white) lcolor(black))
graph export "`dropbox'/gph/figure5.pdf", replace

restore

													**********************************
													**********************************
*---------------------------------------------------**		APPENDIX FIGURES		**---------------------------------------------------*
													**********************************
													**********************************
****************************************
*** FIGURE A.1 - AVERAGE TARIFF RATE ***
****************************************
replace rev_direct = abs(rev_direct)
**Two of the municipalities mis-entered data for the year 2008
replace vol_water_billed = vol_water_billed * 1000 if code7 == 2112233 & year == 2008
replace vol_water_billed = vol_water_billed * 1000 if code7 == 2108207 & year == 2008

egen vol_billed = rowtotal(vol_water_billed vol_sewer_billed)
replace vol_billed = . if vol_water_billed == .

gen tariff = (rev_direct / vol_billed)
*drop figures that SNIS marks as incorrect
replace tariff = . if tariff > 10
replace tariff = . if tariff <= 0 

preserve
drop if balance2001 == 0 
collapse (mean) tariff, by(muni_company year)
twoway (line tariff year if muni_company == 1, sort clpattern(solid)) (line tariff year if muni_company == 0, sort clpattern(dash)), xline(2005, lpat(dot)) xlabel(2001(2)2012,labsize(small)) ylabel(0(0.5)4,labsize(small) angle(horizontal)) ytitle(Rate (in R$ / m{sup:3}), size(small)) xtitle(Year, size(small)) legend( order(1 "Municipal-Run Company" 2 "State-Run Company") size(small)) scheme(s2mono) graphregion(fcolor(white) lcolor(black)) name(FigureA1, replace)
graph export "`dropbox'/gph/figureA1.pdf", replace
restore


***********************************
*** FIGURE A.2 - ELECTION YEARS ***
***********************************
preserve

gen election_year = year

merge 1:1 code election_year using "`dropbox'/election_data.dta"
drop if _merge == 2
drop _merge
drop election_year

gen election_year = 2004 if year>=2001 & year<=2004
replace election_year = 2008 if year>=2005 & year<=2008
replace election_year = 2012 if year>=2009 & year<=2012
keep if year<2013

bys election_year code: egen incumbent1 = sum(incumbent)

*generate the year dummies for the reghdfe command:
*reg invest_total i.year

reghdfe invest_total _Iyear_2002 _Iyear_2003 _Iyear_2004 _Iyear_2005 _Iyear_2006 _Iyear_2007 _Iyear_2008 _Iyear_2009 _Iyear_2010 _Iyear_2011 _Iyear_2012 if balance2001 == 1 & muni_company == 1 & incumbent1 == 1, absorb(code) vce(cluster company)
estimates store Incumbent

reghdfe invest_total _Iyear_2002 _Iyear_2003 _Iyear_2004 _Iyear_2005 _Iyear_2006 _Iyear_2007 _Iyear_2008 _Iyear_2009 _Iyear_2010 _Iyear_2011 _Iyear_2012 if balance2001 == 1 & muni_company == 1 & incumbent1 == 0, absorb(code) vce(cluster company)
estimates store Nonincumbent

coefplot Incumbent Nonincumbent, vertical keep(_Iyear*) coeflabel(_Iyear_2002=2002 _Iyear_2003=2003 _Iyear_2004=2004* _Iyear_2005=2005 _Iyear_2006=2006 _Iyear_2007=2007 _Iyear_2008=2008* _Iyear_2009=2009 _Iyear_2010=2010 _Iyear_2011=2011 _Iyear_2012=2012*) ciopts(color(black)) msize(small) mcolor(black) legend(size(small)) xlabel(,labsize(small)) ylabel(,labsize(small) angle(horizontal)) ytitle(Investment (in ,000 Reals), size(small)) xtitle(Year, size(small)) scheme(s2mono) graphregion(fcolor(white) lcolor(black)) name(FigureA2, replace)
graph export "`dropbox'/gph/figureA2.pdf", replace

restore
